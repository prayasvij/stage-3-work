package com.cognizant.MovieCruiser.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;

@Entity
@Table(name = "fav_movie")
public class Fav_movie {
	@Autowired
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "fm_id")
	private int id;

	@ManyToOne(fetch = FetchType.EAGER)
//	@JoinColumn(name = "us_id") 
	private User user;

	@OneToOne(fetch = FetchType.EAGER)
//	@JoinColumn(name = "fv_mv_id")
//	private List<Integer> movieList;
	private Movie movie;


	public Fav_movie(int id, User user, Movie movie) {
		super();
		this.id = id;
		this.user = user;
		this.movie = movie;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public Fav_movie() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Fav_movie [id=" + id + "]";
	}

//	@Override
//	public String toString() {
//		return "Fav_movie [id=" + id + ", user=" + user + ", movie=" + movie + "]";
//	}

}
