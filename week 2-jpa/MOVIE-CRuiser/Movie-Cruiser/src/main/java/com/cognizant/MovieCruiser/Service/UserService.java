package com.cognizant.MovieCruiser.Service;

import java.util.List;

import com.cognizant.MovieCruiser.model.Movie;
import com.cognizant.MovieCruiser.model.User;

public interface UserService {

	public List<User> findAll();

	public void save(User user);

	public User findById(int id);

	public List<User> findByUsername(String Username);

	public User getUserByUsernameAndPassword(String username, String password);

	public void deleteById(int id);

	public List<Movie> getFavMovie(int id);

}
