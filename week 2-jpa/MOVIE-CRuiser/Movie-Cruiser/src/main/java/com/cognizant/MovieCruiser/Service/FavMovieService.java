package com.cognizant.MovieCruiser.Service;

import java.util.List;

import com.cognizant.MovieCruiser.Service.Exception.FavMovieException;
import com.cognizant.MovieCruiser.model.Fav_movie;

public interface FavMovieService {
	public List<Fav_movie> findAll();

	public void save(Fav_movie favmovie);

	public Fav_movie findById(int id);

	public void deleteById(int id);

	public void updateCart(int id, Fav_movie favmovie) throws FavMovieException;
}
