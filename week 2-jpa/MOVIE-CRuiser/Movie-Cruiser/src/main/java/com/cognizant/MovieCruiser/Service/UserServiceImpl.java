package com.cognizant.MovieCruiser.Service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.MovieCruiser.DAO.UserDAO;
import com.cognizant.MovieCruiser.model.Fav_movie;
import com.cognizant.MovieCruiser.model.Movie;
import com.cognizant.MovieCruiser.model.User;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDAO userDAO;

	public UserServiceImpl(UserDAO theUserDAO) {
		userDAO = theUserDAO;
	}

	@Override
	public List<User> findAll() {
		return userDAO.findAll();
	}

	@Override
	public List<User> findByUsername(String username) {
		List<User> result = userDAO.findUserByUsername(username);
		return result;
	}

	@Override
	public void save(User user) {
		userDAO.save(user);
	}

	@Override
	public User getUserByUsernameAndPassword(String username, String password) {
		return userDAO.getUserByUsernameAndPassword(username, password);
	}

	@Override
	public User findById(int id) {
		return userDAO.getOne(id);
	}

	@Override
	public void deleteById(int id) {
		userDAO.deleteById(id);
	}

	@Override
	public List<Movie> getFavMovie(int id) {
		User user = userDAO.getOne(id);
		List<Movie> l=new ArrayList<Movie>();
		for(Fav_movie f:user.getFav_movie_list())
		{
			l.add(f.getMovie());
		}
		return l;
	}

}
