package com.cognizant.MovieCruiser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.cognizant.MovieCruiser.Service.FavMovieService;
import com.cognizant.MovieCruiser.Service.MovieService;
import com.cognizant.MovieCruiser.Service.UserService;
import com.cognizant.MovieCruiser.model.Fav_movie;
import com.cognizant.MovieCruiser.model.Movie;
import com.cognizant.MovieCruiser.model.User;
import com.cognizant.MovieCruiser.util.DateUtil;

@SpringBootApplication
public class MovieCruiserApplication {
	public static UserService service;
	public static MovieService movieService;
	public static FavMovieService favMovieService;

	public static void main(String[] args) {
		ApplicationContext run = SpringApplication.run(MovieCruiserApplication.class, args);
		service = run.getBean(UserService.class);
		movieService = run.getBean(MovieService.class);
		favMovieService = run.getBean(FavMovieService.class);

		testapp1();
		testapp2();
		testapp3();
		System.out.println("------------------END------------------");
	}

	public static void testapp1() {
		service.save(new User(1, "Prayas", "abc", "prayas", "vijayvargiya", "prayas@abc.com", "user"));
		System.out.println(service.findAll());

	}

	public static void testapp2() {
//
		Movie m1 = new Movie(1, "FAST and the furious 6", "action", DateUtil.convertToDate("10/10/2010"), 7890000d,
				true, true, true,
				"https://hips.hearstapps.com/digitalspyuk.cdnds.net/13/05/movies-fast-and-furious-6-poster.jpg?resize=480:*");
		movieService.save(m1);

		Movie m2 = new Movie(2, "pirate", "action", DateUtil.convertToDate("12/10/2010"), 10000d, false, true, false,

				"abcc.jpg");
		movieService.save(m2);

		Movie m3 = new Movie(3, "harry", "action", DateUtil.convertToDate("12/10/2010"), 10000d, false, false, false,
				"abcc.jpg");
		movieService.save(m3);

//		System.out.println("all" + movieService.findAll());
//		System.out.println("id" + movieService.findById(1));
//		System.out.println("search" + movieService.searchMovie("f"));

	}

	public static void testapp3() {
//		List<Movie> movieList = new ArrayList<Movie>();
//		movieList.add(movieService.findById(1));
//		movieList.add(movieService.findById(3));

		favMovieService.save(new Fav_movie(1, service.findById(1), movieService.findById(1)));
		favMovieService.save(new Fav_movie(2, service.findById(1), movieService.findById(2)));
//		favMovieService.findAll().get(1).setMovieList(movieList);
		System.out.println(favMovieService.findAll());
//		System.out.println(service.findAll());
		System.out.println("favidList"+service.findById(1).getFav_movie_list());
		System.out.println("movieList"+service.getFavMovie(1));

	}
}
