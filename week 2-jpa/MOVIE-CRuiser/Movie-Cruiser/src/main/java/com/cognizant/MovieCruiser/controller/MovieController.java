package com.cognizant.MovieCruiser.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.cognizant.MovieCruiser.Service.MovieService;
import com.cognizant.MovieCruiser.Service.UserService;

@Controller
@SessionAttributes("userId")
public class MovieController {
	@Autowired
	public UserService userService;
	@Autowired
	public MovieService movieService;
//	@Autowired
//	public FavMovieService favMovieService;

	@GetMapping("/")
	public String homePage() {

		return "home";
	}

	@GetMapping("/{type}")
	public String userTypeRedirect(@PathVariable("type") String type, ModelMap modelMap) {
		if (type.equalsIgnoreCase("user")) {
			modelMap.addAttribute("userId", "1");
		}
		modelMap.addAttribute("userId", "1");
		return "home";
	}

	@GetMapping("/view")
	public String userViewMovie(ModelMap model) {
		model.put("movie", movieService.findAll());
		return "viewMovie";
	}

	@ModelAttribute("genre")
	public List<String> listGenre() {
		// auto populate the data from the currency
		ArrayList<String> ctype = new ArrayList<>();
		ctype.add("");
		ctype.add("");
		ctype.add("");
		return ctype;
	}

	@GetMapping("/fav")
	public String favMovie(ModelMap model) {

		model.put("movie", userService.getFavMovie(Integer.parseInt((String) model.get("userId"))));
		return "fav";
	}
}
