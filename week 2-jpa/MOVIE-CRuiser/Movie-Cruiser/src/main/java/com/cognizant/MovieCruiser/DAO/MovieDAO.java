package com.cognizant.MovieCruiser.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.cognizant.MovieCruiser.model.Movie;

public interface MovieDAO extends JpaRepository<Movie, Integer> {
	@Query("From Movie where title = ?1")
	public List<Movie> findByTitle(String title);

	@Query("From Movie where title like %?1%")
	public List<Movie> searchMovie(String find);

}
