package com.cognizant.MovieCruiser.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

//title, box office, active, date of launch, genre (Science Fiction, Superhero, Romance, Comedy, Adventure, Thriller),
//has teaser,fav indicator
@Entity
@Table(name = "movie")
public class Movie {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "mv_id")
	private int id;

	@Column(name = "mv_title", nullable = false, unique = true, length = 25)
	private String title;

	@Column(name = "mv_genre", nullable = false, length = 15)
	private String genre;

	@Column(name = "mv_date_of_launch", nullable = false)
	private Date dateofLaunch;

	@Column(name = "mv_box_office", nullable = false)
	private Double boxOffice;

	@Column(name = "mv_is_Active", nullable = false)
	private boolean active;

	@Column(name = "mv_has_teaser", nullable = false)
	private boolean hasTeaser;

	@Column(name = "mv_favourite", nullable = false)
	private boolean favourite;

	@Column(name = "mv_image_url", nullable = true)
	private String imageUrl;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public Date getDateofLaunch() {
		return dateofLaunch;
	}

	public void setDateofLaunch(Date dateofLaunch) {
		this.dateofLaunch = dateofLaunch;
	}

	public Double getBoxOffice() {
		return boxOffice;
	}

	public void setBoxOffice(Double boxOffice) {
		this.boxOffice = boxOffice;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isHasTeaser() {
		return hasTeaser;
	}

	public void setHasTeaser(boolean hasTeaser) {
		this.hasTeaser = hasTeaser;
	}

	public boolean isFavourite() {
		return favourite;
	}

	public void setFavourite(boolean favourite) {
		this.favourite = favourite;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	@Override
	public String toString() {
		return "Movie [id=" + id + ", title=" + title + ", genre=" + genre + ", dateofLaunch=" + dateofLaunch
				+ ", boxOffice=" + boxOffice + ", active=" + active + ", hasTeaser=" + hasTeaser + ", favourite="
				+ favourite + ", imageUrl=" + imageUrl + "]";
	}

	public Movie(int id, String title, String genre, Date dateofLaunch, Double boxOffice, boolean active,
			boolean hasTeaser, boolean favourite, String imageUrl) {
		super();
		this.id = id;
		this.title = title;
		this.genre = genre;
		this.dateofLaunch = dateofLaunch;
		this.boxOffice = boxOffice;
		this.active = active;
		this.hasTeaser = hasTeaser;
		this.favourite = favourite;
		this.imageUrl = imageUrl;
	}

	public Movie() {
		super();
		// TODO Auto-generated constructor stub
	}

}
