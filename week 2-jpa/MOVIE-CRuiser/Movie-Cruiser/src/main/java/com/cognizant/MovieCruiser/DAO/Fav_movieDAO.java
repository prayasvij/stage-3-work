package com.cognizant.MovieCruiser.DAO;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cognizant.MovieCruiser.model.Fav_movie;

public interface Fav_movieDAO extends JpaRepository<Fav_movie, Integer> {

}
