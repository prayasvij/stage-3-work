package com.cognizant.truyum.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.truyum.model.User;
import com.cognizant.truyum.repositry.UserDAO;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDAO userDAO;

	public UserServiceImpl(UserDAO theUserDAO) {
		userDAO = theUserDAO;
	}

	@Override
	public List<User> findAll() {
		return userDAO.findAll();
	}

	@Override
	public User findByUsername(String Username) {
		Optional<User> result = userDAO.findById(Username);
		User user = null;
		if (result.isPresent()) {
			user = result.get();
		} else {
			throw new RuntimeException("user with id: " + Username + " not found");
		}
		return user;
	}

	@Override
	public void save(User user) {
		userDAO.save(user);
	}

	@Override
	public void deleteByUserName(String Username) {
		userDAO.deleteById(Username);
	}

	@Override
	public User getUserByUsernameAndPassword(String username, String password) {
//		return null;
		return userDAO.getUserByUsernameAndPassword(username, password);

	}

}
