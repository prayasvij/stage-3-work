package com.cognizant.truyum.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.truyum.model.MenuItem;

import com.cognizant.truyum.repositry.MenuItemDAO;
import com.cognizant.truyum.service.Exception.MenuItemException;

@Service
public class MenuItemServiceImpl implements MenuItemService {
	@Autowired
	private MenuItemDAO dao;

	@Transactional
	@Override
	public List<MenuItem> findAll() {

		return (List<MenuItem>) dao.findAll();
	}

	@Transactional
	@Override
	public void save(MenuItem item) {
		dao.save(item);
	}

	@Transactional
	public void updateMenuItem(int id, MenuItem updateMI) throws MenuItemException {
		Optional<MenuItem> result = dao.findById(id);
		if (!result.isPresent()) {
			throw new MenuItemException();
		}
		dao.save(updateMI);

	}

	@Transactional
	@Override
	public MenuItem findById(int id) {
		Optional<MenuItem> result = dao.findById(id);
		MenuItem user = null;
		if (result.isPresent()) {
			user = result.get();
		} else {
			throw new RuntimeException("user with id: " + id + " not found");
		}
		return user;

	}

	@Transactional
	@Override
	public void deleteById(int id) {
		dao.deleteById(id);

	}

	@Transactional
	@Override
	public MenuItem findByName(String name) {

		return dao.findByName(name);
	}

	@Transactional
	@Override
	public List<MenuItem> Search(String name) {
		return dao.findLikeName(name);

	}

}
