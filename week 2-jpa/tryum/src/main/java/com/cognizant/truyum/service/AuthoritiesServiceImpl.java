package com.cognizant.truyum.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.truyum.model.Authorities;
import com.cognizant.truyum.model.MenuItem;
import com.cognizant.truyum.repositry.AuthoritiesDAO;
import com.cognizant.truyum.service.Exception.AuthoritiesException;
import com.cognizant.truyum.service.Exception.MenuItemException;

@Service
public class AuthoritiesServiceImpl implements AuthoritiesService {
	@Autowired
	AuthoritiesDAO dao;

	@Transactional
	@Override
	public List<Authorities> findAll() {
		return (List<Authorities>) dao.findAll();
	}

	@Transactional
	@Override
	public void save(Authorities authorities) {
		dao.save(authorities);

	}

	@Transactional
	@Override
	public Authorities findById(int id) {
		Optional<Authorities> result = dao.findById(id);
		Authorities user = null;
		if (result.isPresent()) {
			user = result.get();
		} else {
			throw new RuntimeException("user with id: " + id + " not found");
		}
		return user;

	}

	@Transactional
	@Override
	public void deleteById(int id) {
		dao.deleteById(id);

	}

	@Transactional
	@Override
	public void updateAuthorities(int id, Authorities authorities) throws AuthoritiesException {
		Optional<Authorities> result = dao.findById(id);
		if (!result.isPresent()) {
			throw new AuthoritiesException();
		}
		dao.save(authorities);

	}

}
