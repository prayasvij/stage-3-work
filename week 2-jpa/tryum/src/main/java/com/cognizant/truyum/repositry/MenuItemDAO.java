package com.cognizant.truyum.repositry;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.cognizant.truyum.model.MenuItem;

@Repository
public interface MenuItemDAO extends CrudRepository<MenuItem, Integer> {
	@Query("From MenuItem where name= ?1")
	public MenuItem findByName(String name);

	@Query("From MenuItem where name like %?1%")
	public List<MenuItem> findLikeName(String name);

}
