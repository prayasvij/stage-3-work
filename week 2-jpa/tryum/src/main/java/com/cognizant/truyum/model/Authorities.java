package com.cognizant.truyum.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "authorities")
public class Authorities {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "au_id")
	private int id;
	@Column(name = "type")
	private String type;
	@OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinColumn(name = "us_username")
	private User user;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Authorities(int id, String type, User user) {
		super();
		this.id = id;
		this.type = type;
		this.user = user;
	}

	public Authorities() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Authorities(int id, String type) {
		super();
		this.id = id;
		this.type = type;
	}

	@Override
	public String toString() {
		return "Authorities [id=" + id + ", type=" + type + "]";
	}

//	@Override
//	public String toString() {
//		return "Authorities [id=" + id + ", type=" + type + ", user=" + user + "]";
//	}

}
