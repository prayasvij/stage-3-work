package com.cognizant.truyum.service;

import java.util.List;

import com.cognizant.truyum.model.Authorities;
import com.cognizant.truyum.service.Exception.AuthoritiesException;

public interface AuthoritiesService {
	public List<Authorities> findAll();

	public void save(Authorities authorities);

	public Authorities findById(int id);

	public void deleteById(int id);

	public void updateAuthorities(int id, Authorities authorities) throws AuthoritiesException;

}
