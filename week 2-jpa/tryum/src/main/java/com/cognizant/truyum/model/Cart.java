package com.cognizant.truyum.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

@EnableAutoConfiguration
@Entity
@Table(name = "cart")
public class Cart {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cart_id")
	private int id;

	@Column(name = "total")
	private double total;

	// fk to username 1-1
	@OneToOne
	@JoinColumn(name = "username")
	private User user;

	// fk to menuitem 1-n

	@OneToOne(fetch = FetchType.EAGER, orphanRemoval = true)
	@JoinColumn(name = "me_id")
	private List<MenuItem> menuItemList;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<MenuItem> getMenuItemList() {
		return menuItemList;
	}

	public void setMenuItemList(List<MenuItem> menuItemList) {
		this.menuItemList = menuItemList;
	}

	public Cart(int id, double total, User user, List<MenuItem> menuItemList) {
		super();
		this.id = id;
		this.total = total;
		this.user = user;
		this.menuItemList = menuItemList;
	}

	public Cart() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Cart(int id, double total) {
		super();
		this.id = id;
		this.total = total;
	}

	@Override
	public String toString() {
		return "Cart [id=" + id + ", total=" + total + ", user=" + user + ", menuItemList=" + menuItemList + "]";
	}

}
