package com.cognizant.truyum.service;

import java.util.List;

import com.cognizant.truyum.model.MenuItem;
import com.cognizant.truyum.service.Exception.MenuItemException;

public interface MenuItemService {
	public List<MenuItem> findAll();

	public List<MenuItem> Search(String name);

	public void save(MenuItem item);

	public MenuItem findById(int id);

	public void deleteById(int id);

	public MenuItem findByName(String name);

	public void updateMenuItem(int id, MenuItem updateMI) throws MenuItemException;

}
