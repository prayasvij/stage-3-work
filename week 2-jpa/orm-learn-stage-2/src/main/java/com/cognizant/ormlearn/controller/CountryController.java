package com.cognizant.ormlearn.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cognizant.ormlearn.model.Country;
import com.cognizant.ormlearn.service.CountryService;

@Controller
//@RequestMapping("/product")
public class CountryController {
	@Autowired
	CountryService countryService;

	@RequestMapping(value = "/", method = RequestMethod.GET)

	public String index() {
		return "index";
	}

	@RequestMapping(value = "/search", method = RequestMethod.GET)
	@ResponseBody
	public List<String> search(HttpServletRequest request) {
		System.out.println(countryService.getAllLike(request.getParameter("term")));
		return countryService.getAllLike(request.getParameter("term"));
//				productService.search(request.getParameter("term"));
	}

}
