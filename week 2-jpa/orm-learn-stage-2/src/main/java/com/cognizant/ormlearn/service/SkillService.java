package com.cognizant.ormlearn.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.ormlearn.model.Employee;
import com.cognizant.ormlearn.model.Skill;
import com.cognizant.ormlearn.repository.SkillRepository;

@Service
public class SkillService {
	@Autowired
	SkillRepository repository;

	@Transactional
	public Skill get(int id) {
//		LOGGER.info("Start");
		return repository.findById(id).get();
	}

	@Transactional
	public void save(Skill skill) {
//	LOGGER.info("Start");
		repository.save(skill);
//	LOGGER.info("End");
	}
}
