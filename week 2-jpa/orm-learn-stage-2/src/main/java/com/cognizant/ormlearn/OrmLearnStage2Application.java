package com.cognizant.ormlearn;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.cognizant.ormlearn.model.Department;
import com.cognizant.ormlearn.model.Employee;
import com.cognizant.ormlearn.model.Skill;
import com.cognizant.ormlearn.repository.EmployeeRepository;
import com.cognizant.ormlearn.service.CountryService;
import com.cognizant.ormlearn.service.DepartmentService;
import com.cognizant.ormlearn.service.EmployeeService;
import com.cognizant.ormlearn.service.SkillService;

@SpringBootApplication
public class OrmLearnStage2Application {

	public static EmployeeService employeeService;
	public static DepartmentService departmentService;
	public static SkillService service;
	private static final Logger LOGGER = LoggerFactory.getLogger(OrmLearnStage2Application.class);

	public static void main(String[] args) {
//		SpringApplication.run(OrmLearnStage2Application.class, args);
		ApplicationContext context = SpringApplication.run(OrmLearnStage2Application.class, args);
		employeeService = context.getBean(EmployeeService.class);
		departmentService = context.getBean(DepartmentService.class);
		service = context.getBean(SkillService.class);
//		testAdd();
//		testGetEmployee();
		testAddSkillToEmployee();
	}

//	private static void testGetEmployee() {
//		LOGGER.info("Start");
//		Employee employee = employeeService.get(1);
////		LOGGER.debug("Employee:{}", employee);
////		LOGGER.debug("Department:{}", employee.getDepartment());
//		LOGGER.debug("Skills:{}", employee.getSkillList());
//		LOGGER.info("End");
//	}
	private static void testAddSkillToEmployee() {
		Employee employee = employeeService.get(15);
		Skill skill = service.get(1);
		Set<Skill> skillList = employee.getSkillList();
//		System.out.println(skillList);
		skillList.add(skill);
		employeeService.save(employee);
	}
//	private static void testAdd() {
//		LOGGER.info("Start");
//
//		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
//		Date d;
//		try {
//			d = formatter.parse("1998-04-10");
//		} catch (ParseException e) {
//			// TODO Auto-generated catch block
//			d = new Date();
//			e.printStackTrace();
//		}
//
//		Department department = departmentService.get(4);
//
//		Employee employee = new Employee(15, "prayas", 10800, true, d, department);
//		employeeService.save(employee);
//		LOGGER.debug("save");
//
//		LOGGER.info("End");
//	}

}
