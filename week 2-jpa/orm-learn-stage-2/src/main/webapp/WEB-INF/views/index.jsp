<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Search Auto Complete with Spring MVC</title>



<script src="/webjars/jquery/1.9.1/jquery.js"></script>
<script type="text/javascript" src="/webjars/jquery-ui/1.12.1/jquery-ui.js"></script>
<link href="/webjars/jquery-ui/1.12.1/jquery-ui.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
	$(document).ready(function() {
		$('#productName').autocomplete({
			source : '${pageContext.request.contextPath }/search'
		});
	});
</script>
</head>
<body>
	Search Product
	<input type="text" id="productName">
</body>
</html>