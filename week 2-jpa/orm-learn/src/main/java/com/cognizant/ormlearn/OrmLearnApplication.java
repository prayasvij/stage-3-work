package com.cognizant.ormlearn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.cognizant.ormlearn.model.Country;
import com.cognizant.ormlearn.service.CountryService;
import com.cognizant.ormlearn.service.exception.CountryNotFoundException;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//private static final Logger LOGGER = LoggerFactory.getLogger(OrmLearnApplication.class);

@SpringBootApplication
public class OrmLearnApplication {

	public static CountryService countryService;
	private static final Logger LOGGER = LoggerFactory.getLogger(OrmLearnApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(OrmLearnApplication.class, args);
//		LOGGER.info("Inside main");
		ApplicationContext context = SpringApplication.run(OrmLearnApplication.class, args);
		countryService = context.getBean(CountryService.class);
//		testGetAllCountries();
//		getAllCountriesTest();
//		testAddCountry();
//		testUpdateCountry();
//		testDeleteCountry();
		testLikeCountry();
	}

	private static void testGetAllCountries() {

//		LOGGER.info("Start");
		List<Country> countries = countryService.getAllCountries();
//		LOGGER.debug("countries={}", countries);
		System.out.print("printed py println" + countries);
//		LOGGER.info("End");
	}

	private static void getAllCountriesTest() {
		LOGGER.info("Start");
		Country country;
		try {
			country = countryService.findCountryByCode("IN");
			LOGGER.debug("Country:{}", country);
		} catch (CountryNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		LOGGER.info("End");
	}

	private static void testAddCountry() {
		LOGGER.info("Start");
		countryService.addCountry(new Country("PK", "pakistan"));
		LOGGER.debug("ADDED SUCCESSFUL");
		LOGGER.info("End");
	}

	private static void testUpdateCountry() {
		LOGGER.info("Start");
		try {
			countryService.updateCountry("PK", "Chota-india");
		} catch (CountryNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		LOGGER.debug("Updated SUCCESSFUL");
		LOGGER.info("End");
	}

	private static void testDeleteCountry() {
		LOGGER.info("Start");

		countryService.deleteCountry("PK");

		LOGGER.debug("Deleted SUCCESSFUL");
		LOGGER.info("End");
	}

	private static void testLikeCountry() {
		LOGGER.info("Start");

		List<String> countries = countryService.getAllLike("ou");
		LOGGER.debug("countries={}", countries);
		LOGGER.debug("Deleted SUCCESSFUL");
		LOGGER.info("End");
	}
}
