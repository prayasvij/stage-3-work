import java.io.*;
import java.math.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;

public class Solution {

    /*
     * Complete the timeConversion function below.
     */
    static String timeConversion(String s) {
        /*
         * Write your code here.
         */
        String converted = new String();
        String givenTime[] = s.split(":");
        String format = givenTime[2].substring(2,4);
        int a = Integer.parseInt(givenTime[0]);
        int min = Integer.parseInt(givenTime[1]);
        int sec = Integer.parseInt(givenTime[2].substring(0,2));    
            
        if(format.equals("PM")){
            if(givenTime[0].equals("12")){
                           converted = "12" + ":" + givenTime[1] + ":" + givenTime[2].substring(0,2);
                     
            }
            else{
                a = a + 12;
                converted = Integer.toString(a) + ":" + givenTime[1] + ":" + givenTime[2].substring(0,2);
            }
        }
               
        if(format.equals("AM")){
            if(givenTime[0].equals("12")){
                
                    converted = "00" + ":" + givenTime[1] + ":" + givenTime[2].substring(0,2);    
                
            }
           else{
                    converted = givenTime[0] + ":" + givenTime[1] + ":" + givenTime[2].substring(0,2);
           }
        }
            
       return converted; 


    }

    private static final Scanner scan = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String s = scan.nextLine();

        String result = timeConversion(s);

        bw.write(result);
        bw.newLine();

        bw.close();
    }
}
